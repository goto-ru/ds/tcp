{-# LANGUAGE OverloadedStrings #-}

module Main where

import Network.Socket hiding (send, sendTo, recv, recvFrom)
import Network.Socket.ByteString
import Control.Concurrent
import Control.Exception
import Control.Monad
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as C

main :: IO ()
main = bracket
        (do
            ssock <- mkSock
            listen ssock 1
            return ssock)
        close
        (\ssock -> do
            (csock, addr) <- accept ssock
            finally (talk csock addr []) (close csock))
        

mkSock :: IO Socket
mkSock = do
    sock <- socket AF_INET6 Stream defaultProtocol
    setSocketOption sock ReuseAddr 1
    setSocketOption sock ReusePort 1
    setSocketOption sock IPv6Only 1
    bind sock $ SockAddrInet6 3000 0 iN6ADDR_ANY 0
    return sock

talk :: Socket -> SockAddr -> [B.ByteString] -> IO ()
talk sock peer state = do
    sendAll sock $ C.pack $ show $ length state
    threadDelay 100000
    talk sock peer ("mow" : state)
