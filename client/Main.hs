{-# LANGUAGE OverloadedStrings #-}

module Main where

import Control.Monad
import Control.Exception
import qualified Data.ByteString.Char8 as C
import Network.Socket hiding (send, sendTo, recv, recvFrom)
import Network.Socket.ByteString (send, recv)

main :: IO ()
main = do
    addrInfo <- getAddrInfo Nothing (Just "localhost") (Just "3000")
    let serverAddr = head addrInfo
    bracket
        (do
            sock <- socket (addrFamily serverAddr) Stream defaultProtocol
            connect sock (addrAddress serverAddr)
            return sock)
        close
        sendMsg

sendMsg :: Socket -> IO ()
sendMsg sock = do
    void $ send sock "Hello"
    C.putStrLn =<< recv sock 16
