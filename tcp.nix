{ mkDerivation, base, bytestring, network, stdenv }:
mkDerivation {
  pname = "tcp";
  version = "0.0.0.1";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [ base bytestring network ];
  homepage = "https://gitlab.com/goto-ru/ds/tcp";
  license = stdenv.lib.licenses.agpl3;
}
