tcp
===

A simple TCP server / client demo.

Running
-------

.. code:: sh

    1$ nix-shell
    1$ cabal --disable-nix run tcp-server # starts a server on port 3000

    2$ nix-shell
    2$ telnet localhost 3000
    2$ cabal --disable-nix exec tcp-client
